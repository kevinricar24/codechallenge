# Code Challenge

code challenge to manage file repository [cloud and local]
- Net Core Console Application
- Dropbox.Api
- Visual Studio Test Tool
- Cross Platform (Windows, Linux, MacOs)

## Command
- Parameter Required
- Parameter Optional

```cmd
  - dotnet run exists /root/file.extension
  - dotnet run get 	/root/file.extension -o ./path/file.extension
  - dotnet run put    /root/file.extension @./path/file.extension
```

## Paths

dotnet run get /root/red.png 	-o /temp/newRed.png

- Absolute 	/ 	= /temp/newRed.png
- Relative	./ 	= /demo/codechallenge/temp/newRed.png
- Relative	../ 	= /demo/temp/newRed.png


# Actions
![alt text](codechallenge/Images/actions.png "Actions")

# Unit Test
![alt text](codechallenge/Images/unit-test.png "Unit Test")