using codechallenge.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace codechallenge.Test
{
    [TestClass]
    public class RepositoryExistTest : RepositoryTestBase
    {
        [TestMethod]
        public void ValidateCommandWithOutActions()
        {
            string[] args = new string[] { };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.CommandNotValid(), response);
        }

        [TestMethod]
        public void ValidateExistCommandWithOutParameter()
        {
            string[] args = new string[] { "exists" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.CommandNotValid(), response);
        }

        [TestMethod]
        public void ValidateExistCommandWithPathFormatNotValid()
        {
            string[] args = new string[] { "exists", "c:\\test\\myFile.png" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.RepositoryPathFormatNotValid(), response);
        }

        [TestMethod]
        public void ValidateFileNotExist()
        {
            string[] args = new string[] { "exists", "/root/TestingPink.png" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.FAIL(), response);
        }

        [TestMethod]
        public void ValidateFileNotExistWithError()
        {
            managerRepository.tokenDropbox = "testingNewToken";
            managerRepository.repositoryFolder = null;
            string[] args = new string[] { "exists", "/@--images/Test.png" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.RepositoryPathFormatNotValid(), response);
        }

        [TestMethod]
        public void ValidateExistFile()
        {
            string cloudRepository = "/root/blue.png";
            string fileSize = "386";
            string[] args = new string[] { "exists", cloudRepository };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.OK(cloudRepository, fileSize), response);
        }
    }
}
