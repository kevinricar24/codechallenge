﻿using codechallenge.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace codechallenge.Test
{
    [TestClass]
    public class RepositoryGetTest : RepositoryTestBase
    {

        [TestMethod]
        public void ValidateGetCommandWithOutParameter()
        {
            string[] args = new string[] { "get" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.CommandNotValid(), response);
        }

        [TestMethod]
        public void ValidateGetCommandWithPathFormatNotValid()
        {
            string[] args = new string[] { "get", "c:\\test\\myFile.png" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.RepositoryPathFormatNotValid(), response);
        }

        [TestMethod]
        public void ValidateGetCommandWithPrefixNotValid()
        {
            string[] args = new string[] { "get", "/images/test.png", "-k"};

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.CommandNotValid(), response);
        }

        [TestMethod]
        public void ValidateGetFileNotExists()
        {
            string[] args = new string[] { "get", "/root/darkblue.png" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.FAIL(), response);
        }

        [TestMethod]
        public void ValidateGetFile()
        {
            string cloudRepository = "/root/blue.png";
            string fileSize = "386";
            string fullPathName = string.Format("{0}{1}{2}", Directory.GetCurrentDirectory(), Validators.backSlash, Path.GetFileName(cloudRepository));
            fullPathName = fullPathName.Replace("C:", string.Empty);
            fullPathName = fullPathName.Replace(Validators.backSlash, Validators.slash);
            string[] args = new string[] { "get", cloudRepository };
            
            string response = new Process(managerRepository).Execute(args);
            
            Assert.AreEqual(Messages.OK(fullPathName, fileSize), response);
        }

    }
}
