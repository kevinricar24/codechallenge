﻿using codechallenge.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace codechallenge.Test
{
    [TestClass]
    public class RepositoryPutTest : RepositoryTestBase
    {
        [TestMethod]
        public void ValidatePutCommandWithOutParameter()
        {
            string[] args = new string[] { "put" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.CommandNotValid(), response);
        }

        [TestMethod]
        public void ValidatePutCommandWithPathFormatNotValid()
        {
            string[] args = new string[] { "put", "c:\\test\\myFile.png", "file.pdf" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.RepositoryPathFormatNotValid(), response);
        }

        [TestMethod]
        public void ValidatePutCommandWithPrefixNotValid()
        {
            string[] args = new string[] { "put", "/root/images/test.png", "-/test.pdf" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.LocalPathFormatNotValid("@"), response);
        }

        [TestMethod]
        public void ValidatePutCommandWithFileNotExist()
        {
            string[] args = new string[] { "put", "/root/images/test.png", "@/testing.pdf" };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.FAIL(), response);
        }   

        [TestMethod]
        public void ValidatePutCommandWithFileExist()
        {
            string cloudRepository = "/root/UnitTest/blue.png";

            #if (!DEBUG)
            string format = Validators.IsWindows() ? Validators.backSlash : Validators.slash;
            string additionalPath = string.Format("bin{0}Debug{0}netcoreapp2.2", format);
            string directory = Directory.GetCurrentDirectory().Replace(additionalPath, string.Empty);
            #else
            string directory = Directory.GetCurrentDirectory() + Validators.slash;
            #endif

            string fullPathName = string.Format("@{0}{1}", directory, "blue.png");
            fullPathName = Validators.UnixFormatFilePath(fullPathName);
            string fileSize = "386";
            string[] args = new string[] { "put", cloudRepository, fullPathName };

            string response = new Process(managerRepository).Execute(args);

            Assert.AreEqual(Messages.OK(cloudRepository, fileSize), response);
        }

    }
}
