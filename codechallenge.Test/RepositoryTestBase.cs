﻿using codechallenge.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace codechallenge.Test
{
    [TestClass]
    public class RepositoryTestBase
    {
        public IManagerRepository managerRepository;

        [TestInitialize]
        public void PopulateData()
        {
            managerRepository = new ManagerRepository();
            managerRepository.tokenDropbox = "uHCoWxthkIAAAAAAAAAAZKLixK1FpYtjNC-z6n3A_8RE2iO2q3U3LRlI82UiFf77";
            managerRepository.repositoryFolder = "root";
        }
    }
}
