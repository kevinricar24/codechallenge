﻿namespace codechallenge
{
    public class FileDetails
    {
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public bool FileExistInCloudRepository { get; set; }
        public bool FileExistInLocalRepository { get; set; }
        public string FilePathInCloudRepository { get; set; }
        public string FilePathInLocalRepository { get; set; }
        public string FileOutputAction { get; set; }
    }
}
