﻿using codechallenge.Repository;
using codechallenge.Utils;
using System;

namespace codechallenge
{
    public class Process
    {
        
        private readonly IManagerRepository _managerRepository;

        public Process(IManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
        }

        public string Execute(string[] input)
        {
            Tuple<string, string> response;
            Validators validators = new Validators(_managerRepository);
            response = validators.ValidateParameters(input);
            if (!string.IsNullOrEmpty(response.Item1)){
                FileDetails fileDetails = new FileDetails();
                string action = input[0];
                string filePath1 = response.Item1;
                string filePath2 = response.Item2;

                if (action == Actions.put.ToString())
                {
                    var upload = _managerRepository.Put(fileDetails, filePath1, filePath2);
                    upload.Wait();
                    fileDetails = upload.Result;
                    return fileDetails.FileOutputAction;
                }
                else
                {
                    var exist = _managerRepository.Exists(filePath1);
                    exist.Wait();
                    fileDetails = exist.Result;

                    if (fileDetails.FileExistInCloudRepository || fileDetails.FileExistInLocalRepository)
                    {
                        if (action == Actions.get.ToString())
                        {
                            var download = _managerRepository.Get(fileDetails, filePath2);
                            download.Wait();
                            fileDetails = download.Result;
                        }
                    }

                    return fileDetails.FileOutputAction;
                }
            }
            return response.Item2;
        }
    }
}
