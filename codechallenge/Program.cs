﻿using codechallenge.Repository;
using codechallenge.Utils;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace codechallenge
{
    public class Program
    {
        public static string _tokenDropbox { get; set; }
        public static string _repositoryFolder { get; set; }

        public static void Main(string[] args)
        {
            string response = string.Empty;
            Tuple<bool, string> initKeys = ConfigurationExists();
            response = initKeys.Item2;
            if (initKeys.Item1)
            {
                IManagerRepository managerRepository = new ManagerRepository();
                managerRepository.tokenDropbox = _tokenDropbox;
                managerRepository.repositoryFolder = _repositoryFolder;
                
                Process process = new Process(managerRepository);
                response = process.Execute(args);
            }
            Messages.PrintMessage(response);
        }

        private static Tuple<bool, string> ConfigurationExists()
        {

#if (!DEBUG)
            string format = Validators.IsWindows() ? Validators.backSlash : Validators.slash;
            string additionalPath = string.Format("bin{0}Debug{0}netcoreapp2.2", format);
            string directory = Directory.GetCurrentDirectory().Replace(additionalPath, string.Empty);
#else
            string directory = Directory.GetCurrentDirectory();
#endif

            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(directory)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .Build();

            string dropboxAPISection = "DropboxAPI";
            string tokenDropboxKey = "token";
            string repositorySection = "Repository";
            string repositoryPathKey = "folder";

            Tuple<bool, string> outputValidation;
            outputValidation = Validators.ValidateConfigurationExist(configuration, dropboxAPISection, tokenDropboxKey);
            if (outputValidation.Item1)
            {
                _tokenDropbox = outputValidation.Item2;
                outputValidation = Validators.ValidateConfigurationExist(configuration, repositorySection, repositoryPathKey);
                if (outputValidation.Item1)
                {
                    _repositoryFolder = outputValidation.Item2;
                    string localDirectory = localDirectory = string.Format("{0}{1}{2}", Directory.GetCurrentDirectory(),
                            Validators.slash, _repositoryFolder);
                    outputValidation = Validators.ValidateRepository(localDirectory);
                }
            }

            return outputValidation;
        }
    }

}
