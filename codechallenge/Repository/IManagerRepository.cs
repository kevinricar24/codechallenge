﻿using System.Threading.Tasks;

namespace codechallenge.Repository
{
    public interface IManagerRepository
    {
        string tokenDropbox { get; set; }
        string repositoryFolder { get; set; }

        Task<FileDetails> Exists(string filePath);

        Task<FileDetails> Get(FileDetails fileDetails, string outputPath);

        Task<FileDetails> Put(FileDetails fileDetails, string outputPath, string filePath);
    }
}
