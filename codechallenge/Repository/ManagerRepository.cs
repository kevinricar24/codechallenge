﻿using codechallenge.Utils;
using Dropbox.Api;
using Dropbox.Api.Files;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace codechallenge.Repository
{
    public class ManagerRepository : IManagerRepository
    {
        public string tokenDropbox { get; set; }
        public string repositoryFolder { get; set; }

        public async Task<FileDetails> Exists(string filePath)
        {
            Messages.PrintMessage(Messages.SearchProgress());
            FileDetails fileDetails = new FileDetails();
            string fileToSearch = string.Empty;
            fileDetails = new FileDetails() { FileName = Path.GetFileName(filePath) };

            fileDetails = await OperationsCloudRepository(Actions.exists, fileDetails, filePath, string.Empty);
            if (!fileDetails.FileExistInCloudRepository)
            {
                //Search on the local machine
                try
                {
                    fileToSearch = string.Format("{0}{1}", Directory.GetCurrentDirectory(), filePath);
                    fileToSearch = Validators.UnixFormatFilePath(fileToSearch);
                    FileInfo fi = new FileInfo(fileToSearch);
                    if (fi.Exists)
                    {
                        fileDetails.FileSize = fi.Length.ToString();
                        fileDetails.FileExistInLocalRepository = true;
                        fileDetails.FilePathInLocalRepository = fileToSearch;
                        fileDetails.FileOutputAction = Messages.OK(filePath, fi.Length.ToString());
                    }
                    else
                    {
                        fileDetails.FileOutputAction = Messages.FAIL();
                    }
                }
                catch (Exception ex)
                {
                    fileDetails.FileOutputAction = Messages.FAILWithError(ex.Message);
                }
            }

            return fileDetails;
        }

        public async Task<FileDetails> Get(FileDetails fileDetails, string outputPath)
        {
            Messages.PrintMessage(Messages.DownloadProgress());
            if (fileDetails.FileExistInCloudRepository)
            {
                fileDetails = await OperationsCloudRepository(Actions.get, fileDetails, string.Empty, outputPath);
            }
            else if (fileDetails.FileExistInLocalRepository)
            {
                try
                {
                    if (!Directory.Exists(outputPath))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(outputPath));
                    }
                    File.Copy(fileDetails.FilePathInLocalRepository, outputPath, true);
                    fileDetails.FileOutputAction = Messages.OK(outputPath, fileDetails.FileSize);
                }
                catch (Exception ex)
                {
                    fileDetails.FileOutputAction = Messages.FAILWithError(ex.Message);
                }
            }
            return fileDetails;
        }

        public async Task<FileDetails> Put(FileDetails fileDetails, string outputPath, string filePath)
        {
            Messages.PrintMessage(Messages.SearchProgress());
            FileInfo fi = new FileInfo(filePath);
            if (fi.Exists)
            {
                Messages.PrintMessage(Messages.UploadProgress());
                fileDetails = await OperationsCloudRepository(Actions.put, fileDetails, filePath, outputPath);
                if (!fileDetails.FileExistInCloudRepository)
                {
                    try
                    {
                        string localpath = string.Format("{0}{1}", Directory.GetCurrentDirectory(), outputPath);
                        localpath = Validators.UnixFormatFilePath(localpath);
                        fileDetails.FileName = Path.GetFileName(localpath);
                        fileDetails.FileSize = fi.Length.ToString();
                        fileDetails.FileExistInLocalRepository = true;
                        fileDetails.FilePathInLocalRepository = localpath;
                        if (!Directory.Exists(localpath))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(localpath));
                        }
                        fi.CopyTo(fileDetails.FilePathInLocalRepository, true);
                        fileDetails.FileOutputAction = Messages.OK(outputPath, fileDetails.FileSize);
                    }
                    catch (Exception ex)
                    {
                        fileDetails.FileOutputAction = Messages.FAILWithError(ex.Message);
                    }
                }
            }
            else
            {
                fileDetails.FileOutputAction = Messages.FAIL();
            }
            return fileDetails;
        }

        private async Task<FileDetails> OperationsCloudRepository(Actions action, FileDetails fileDetails,
            string filePath, string outputPath)
        {
            try
            {
                DropboxClient dropboxClient;

                using (dropboxClient = new DropboxClient(tokenDropbox))
                {
                    if (action == Actions.exists)
                    {
                        var list = await dropboxClient.Files.ListFolderAsync(string.Empty, true);
                        var fileInCloud = list.Entries.Where(i => i.IsFile && i.PathDisplay == filePath)
                                                      .FirstOrDefault();
                        if (fileInCloud != null)
                        {
                            fileDetails.FileExistInCloudRepository = true;
                            fileDetails.FilePathInCloudRepository = filePath;
                            fileDetails.FileSize = fileInCloud.AsFile.Size.ToString();
                            fileDetails.FileOutputAction = Messages.OK(fileDetails.FilePathInCloudRepository, fileDetails.FileSize);
                        }
                    }
                    else if (action == Actions.get)
                    {
                        using (var response = await dropboxClient.Files.DownloadAsync(fileDetails.FilePathInCloudRepository))
                        {
                            var s = response.GetContentAsByteArrayAsync();
                            s.Wait();
                            byte[] fileInBytes = s.Result;
                            if (!Directory.Exists(outputPath))
                            {
                                Directory.CreateDirectory(Path.GetDirectoryName(outputPath));
                            }
                            File.WriteAllBytes(outputPath, fileInBytes);
                            fileDetails.FileOutputAction = Messages.OK(outputPath, fileDetails.FileSize);
                        }
                    }
                    else if (action == Actions.put)
                    {
                        using (var mem = new MemoryStream(File.ReadAllBytes(filePath)))
                        {
                            string fileSize = mem.Length.ToString();
                            var updated = dropboxClient.Files.UploadAsync(outputPath, WriteMode.Overwrite.Instance, body: mem);
                            updated.Wait();
                            //var tx = dropboxClient.Sharing.CreateSharedLinkWithSettingsAsync(outputPath);
                            //tx.Wait();
                            //string url = tx.Result.Url;

                            fileDetails.FileName = Path.GetFileName(outputPath);
                            fileDetails.FileSize = fileSize;
                            fileDetails.FileExistInCloudRepository = true;
                            fileDetails.FilePathInCloudRepository = outputPath;
                            fileDetails.FileOutputAction = Messages.OK(fileDetails.FilePathInCloudRepository, fileDetails.FileSize);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fileDetails.FileOutputAction = Messages.FAILWithError(ex.Message);
            }

            return fileDetails;
        }
    }
}
