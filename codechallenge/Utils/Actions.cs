﻿using System.ComponentModel;

namespace codechallenge.Utils
{
    enum Actions
    {
        [Description("exists")]
        exists = 0,
        [Description("get")]
        get = 1,
        [Description("put")]
        put = 2
    }
}
