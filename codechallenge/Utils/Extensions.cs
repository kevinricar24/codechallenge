﻿using System;
using System.ComponentModel;
using System.Linq;

namespace codechallenge.Utils
{
    public static class Extensions
    {
        public static string GetDescription<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                var memInfo = type.GetMember(type.GetEnumName(e));
                var descriptionAttribute = memInfo[0]
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .FirstOrDefault() as DescriptionAttribute;

                if (descriptionAttribute != null)
                {
                    return descriptionAttribute.Description;
                }
            }
            return string.Empty;
        }
    }
}
