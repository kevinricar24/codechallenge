﻿using System;

namespace codechallenge.Utils
{
    public class Messages
    {
        private const string existsFormat = "exists <file>";
        private const string getFormat = "get <file> -o output_path";
        private const string putFormat = "put <file> @<local_file>";
        private const string absolutePathFormat = "/path/../file.extension";
        private const string relativePathFormat = "./path/../file.extension";
        private const string relativePreviousPathFormat = "../path/../file.extension";

        public static string SearchProgress()
        {
            return string.Format("Searching...");
        }

        public static string DownloadProgress()
        {
            return string.Format("Downloading...");
        }

        public static string UploadProgress()
        {
            return string.Format("Uploading...");
        }

        public static string ConfigurationNotExists(string key)
        {
            return string.Format("Error: Required configuration, please add {0} into appsettings.json", key);
        }

        public static string CommandNotValid()
        {
            return string.Format("Warning: Command not valid, please use: \n{0}\n{1}\n{2}", existsFormat, getFormat, putFormat);
        }

        public static string RepositoryPathFormatNotValid()
        {
            return string.Format("Warning: Parameter of path not valid, use this structure /rootPathRepository/../file.extension");
        }

        public static string LocalPathFormatNotValid(string symbol)
        {
            return string.Format("Warning: Parameter of path not valid, use this structure: \n{0}{1}\n{0}{2}\n{0}{3}", symbol, absolutePathFormat, relativePathFormat, relativePreviousPathFormat);
        }

        public static string OK(string fileFullPathName, string fileSize)
        {
            return string.Format("OK ({0}, {1}B)", fileFullPathName.Replace("\\", "/"), fileSize);
        }

        public static string FAIL()
        {
            return string.Format("FAIL (ENOENT, file not found)");
        }

        public static string FAILWithError(string errorMessage)
        {
            return string.Format("FAIL ({0})", errorMessage);
        }

        public static void PrintMessage(string response)
        {
            if (response.Contains(StatusResponse.Error.GetDescription()))
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (response.Contains(StatusResponse.Warning.GetDescription()))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            else if (response.Contains(StatusResponse.FAIL.GetDescription()))
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
            }
            else if (response.Contains(StatusResponse.OK.GetDescription()))
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }

            Console.WriteLine(response);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
