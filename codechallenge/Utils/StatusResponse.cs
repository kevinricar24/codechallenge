﻿using System.ComponentModel;

namespace codechallenge.Utils
{
    enum StatusResponse
    {
        [Description("Error")]
        Error = 0,
        [Description("Warning")]
        Warning = 1,
        [Description("FAIL")]
        FAIL = 2,
        [Description("OK")]
        OK = 3
    }
}
