﻿using codechallenge.Repository;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace codechallenge.Utils
{
    public class Validators
    {
        public static string slash = @"/";
        public static string backSlash = @"\";
        private string optional = "-o";
        private string atpath = "@";
        private string path1 = "repository";
        private string path2 = "local";

        private readonly IManagerRepository _managerRepository;

        public Validators(IManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
        }

        public static Tuple<bool, string> ValidateConfigurationExist(IConfigurationRoot configuration, string section, string children)
        {
            string jsonFormat = string.Format("\"{0}\":{{\"{1}\":\"{2}\"}}", section, children, "value");

            if (configuration.GetSection(section).Exists())
            {
                if (configuration.GetSection(section).GetSection(children).Exists())
                {
                    string childrenValue = configuration.GetSection(section).GetSection(children).Value;
                    if (!string.IsNullOrEmpty(childrenValue))
                    {
                        return Tuple.Create(true, childrenValue);
                    }
                }
            }
            Messages.PrintMessage(Messages.ConfigurationNotExists(jsonFormat));

            return Tuple.Create(false, string.Empty);
        }
        public Tuple<string, string> ValidateParameters(string[] param)
        {
            Tuple<string, string> response;
            response = Tuple.Create(string.Empty, Messages.CommandNotValid());
            if(param.Length >= 2 && param.Length <= 4)
            {
                response = ValidateActions(param);
            }
            else
            {
                response = Tuple.Create(string.Empty, Messages.CommandNotValid());
            }
            return response;
        }
        private Tuple<string, string> ValidateActions(string[] param)
        {
            Tuple<string, string> response;
            if (param[0] == Actions.exists.GetDescription() && param.Length == 2)
            {
                string output = ValidateFormatPath(param[1], path1, param[0]);
                if(output == Messages.RepositoryPathFormatNotValid())
                {
                    response = Tuple.Create(string.Empty, output);
                }
                else
                {
                    response = Tuple.Create(output, string.Empty);
                }
            }
            else if (param[0] == Actions.get.GetDescription() && (param.Length == 2 || param.Length == 4))
            {
                string repoPath = ValidateFormatPath(param[1], path1, param[0]);
                string localPath = string.Empty;
                if (!repoPath.Equals(Messages.RepositoryPathFormatNotValid()))
                {
                   localPath = string.Format("{0}\\{1}", Directory.GetCurrentDirectory(), Path.GetFileName(repoPath));
                   localPath = UnixFormatFilePath(localPath);
                    if (param.Length == 4)
                    {
                        if (ValidatePrefix(param[2], optional))
                        {
                            localPath = ValidateFormatPath(param[3], path2, param[0]);
                        }
                    }

                    if (!localPath.Equals(Messages.LocalPathFormatNotValid(string.Empty)))
                    {
                        response = Tuple.Create(repoPath, localPath);
                    }
                    else
                    {
                        response = Tuple.Create(string.Empty, localPath);
                    }
                }
                else
                {
                    response = Tuple.Create(string.Empty, repoPath);
                }
            }
            else if (param[0] == Actions.put.GetDescription() && param.Length == 3)
            {
                string repoPath = ValidateFormatPath(param[1], path1, param[0]);
                string localPath = string.Empty;
                if (!repoPath.Equals(Messages.RepositoryPathFormatNotValid()))
                {
                    localPath = Messages.LocalPathFormatNotValid(atpath);
                    string inputPrefix = param[2].Substring(0, 1);
                    if (ValidatePrefix(inputPrefix, atpath))
                    {
                        string parameter = param[2].Replace(atpath, string.Empty);
                        localPath = ValidateFormatPath(parameter, path2, param[0]);
                    }
                    if (!localPath.Equals(Messages.LocalPathFormatNotValid(atpath)))
                    {
                        response = Tuple.Create(repoPath, localPath);
                    }
                    else
                    {
                        response = Tuple.Create(string.Empty, localPath);
                    }
                }
                else
                {
                    response = Tuple.Create(string.Empty, repoPath);
                }
            }
            else
            {
                response = Tuple.Create(string.Empty, Messages.CommandNotValid());
            }
            return response;
        }
        private string ValidateFormatPath(string filePath, string fileLocation, string action)
        {
            string response = string.Empty;
            string symbol = string.Empty;
            if (fileLocation.Equals(path1))
            {
                response = Messages.RepositoryPathFormatNotValid();
            }
            else if (fileLocation.Equals(path2))
            {
                if(action == Actions.put.GetDescription())
                {
                    symbol = atpath;
                }
                response = Messages.LocalPathFormatNotValid(symbol);
            }
            if (!filePath.Contains(backSlash))
            {
                if (!string.IsNullOrEmpty(Path.GetFileName(filePath)) && Path.GetFileName(filePath).Contains("."))
                {
                    if (fileLocation.Equals(path1))
                    {
                        string rootPath = string.Format("/{0}/", _managerRepository.repositoryFolder);
                        if (filePath.StartsWith(rootPath))
                        {
                            response = filePath;
                        }
                        else
                        {
                            response = Messages.RepositoryPathFormatNotValid();
                        }
                    }
                    else if (fileLocation.Equals(path2))
                    {
                        string output = string.Empty;
                        string myDirectory = Directory.GetCurrentDirectory();
                        myDirectory = UnixFormatFilePath(myDirectory);
                        if (filePath.StartsWith("/"))
                        {
                            output = filePath;
                        }
                        else if (filePath.StartsWith("./"))
                        {
                            filePath = filePath.Substring(2, filePath.Length - 2);
                            output = string.Format("{0}{1}{2}", myDirectory, slash, filePath);
                        }
                        else if (filePath.StartsWith("../"))
                        {
                            filePath = filePath.Substring(3, filePath.Length - 3);
                            myDirectory = myDirectory.Substring(0, myDirectory.LastIndexOf(slash));
                            output = string.Format("{0}{1}{2}", myDirectory, slash, filePath);
                        }
                        else
                        {
                            output = Messages.LocalPathFormatNotValid(symbol);
                        }
                        response = output;
                    }
                }
            }
            return response;
        }
        private bool ValidatePrefix(string input, string prefix)
        {
            return input == prefix ? true : false;
        }
        public static Tuple<bool, string> ValidateRepository(string localDirectory)
        {
            Tuple<bool, string> output;
            try
            {
                if (!Directory.Exists(localDirectory))
                {
                    Directory.CreateDirectory(localDirectory);
                }
                output = Tuple.Create(true, string.Empty);
            }
            catch (Exception ex)
            {
                output = Tuple.Create(false, Messages.FAILWithError(ex.Message));
            }
            return output;
        }
        public static string UnixFormatFilePath(string path)
        {
            if (path.Contains("C:"))
            {
                path = path.Replace("C:", string.Empty);
            }
            path = path.Replace(backSlash, slash);
            return path;
        }
        public static string WindowsFormatFilePath(string path)
        {
            path = path.Replace(slash, backSlash);
            return path;
        }
        public static bool IsWindows()
        {
            try
            {
                OperatingSystem os_info = Environment.OSVersion;
                if (os_info.Platform.ToString().ToLower().Contains("win"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
